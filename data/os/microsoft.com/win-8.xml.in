<libosinfo version="0.0.1">
<!-- Licensed under the GNU General Public License version 2 or later.
     See http://www.gnu.org/licenses/ for a copy of the license text -->
  <os id="http://microsoft.com/win/8">
    <short-id>win8</short-id>
    <_name>Microsoft Windows 8</_name>
    <version>6.2</version>
    <_vendor>Microsoft Corporation</_vendor>
    <family>winnt</family>
    <distro>win</distro>
    <derives-from id="http://microsoft.com/win/7"/>
    <upgrades id="http://microsoft.com/win/7"/>

    <release-date>2012-09-01</release-date>
    <eol-date>2018-01-09</eol-date>

    <variant id="debug">
      <_name>Microsoft Windows 8 Debug Check Build</_name>
    </variant>
    <variant id="enterprise">
      <_name>Microsoft Windows 8 Enterprise</_name>
    </variant>
    <variant id="enterprise-debug">
      <_name>Microsoft Windows 8 Enterprise Debug Check Build</_name>
    </variant>
    <variant id="professional">
      <_name>Microsoft Windows 8 Professional</_name>
    </variant>

    <!-- No Variant -->
    <media arch="i686" installer-reboots="2">
      <iso>
        <volume-id>(HB1_CCPA_X86FRE|HRM_CCSN?A_X86FRE)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>
    <media arch="x86_64" installer-reboots="2">
      <iso>
        <volume-id>(HB1_CCPA_X64FREE?|HRM_CCSN?A_X64FREE?)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>

    <!-- Debug -->
    <media arch="i686" installer-reboots="2">
      <variant id="debug"/>
      <iso>
        <volume-id>(HRM_CCSN?A_X86CHK)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>
    <media arch="x86_64" installer-reboots="2">
      <variant id="debug"/>
      <iso>
        <volume-id>(HRM_CCSN?A_X64CHK)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>

    <!-- Enterprise -->
    <media arch="i686" installer-reboots="2">
      <variant id="enterprise"/>
      <iso>
        <volume-id>(HRM_CENN?A_X86FREV)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>
    <media arch="x86_64" installer-reboots="2">
      <variant id="enterprise"/>
      <iso>
        <volume-id>(HRM_CENN?A_X64FREV)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>

    <!-- Enterprise Debug -->
    <media arch="i686" installer-reboots="2">
      <variant id="enterprise-debug"/>
      <iso>
        <volume-id>(HRM_CENN?A_X86CHKV)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>
    <media arch="x86_64" installer-reboots="2">
      <variant id="enterprise-debug"/>
      <iso>
        <volume-id>(HRM_CENN?A_X64CHKV)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>

    <!-- Professional -->
    <media arch="i686" installer-reboots="2">
      <variant id="professional"/>
      <iso>
        <volume-id>(HRM_CPRN?A_X86FREV)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>
    <media arch="x86_64" installer-reboots="2">
      <variant id="professional"/>
      <iso>
        <volume-id>(HRM_CPRN?A_X64FREV)_</volume-id>
        <publisher-id>MICROSOFT CORPORATION</publisher-id>
        <l10n-language regex="true" l10n-language-map="http://microsoft.com/win/8/l10n-language">[A-Z0-9_]*_([A-Z]*-[A-Z]*)</l10n-language>
      </iso>
    </media>

    <resources arch="i686">
      <minimum>
        <cpu>1000000000</cpu>
        <n-cpus>1</n-cpus>
        <ram>1073741824</ram>
        <storage>17179869184</storage>
      </minimum>
    </resources>

    <resources arch="x86_64">
      <minimum>
        <cpu>1000000000</cpu>
        <n-cpus>1</n-cpus>
        <ram>2147483648</ram>
        <storage>21474836480</storage>
      </minimum>

      <maximum>
        <ram>549755813888</ram>
      </maximum>
    </resources>

    <devices>
      <device id="http://pcisig.com/pci/1033/0194"/> <!-- nec-xhci -->
      <device id="http://pcisig.com/pci/1b36/0004"/> <!-- qemu-xhci -->
    </devices>

    <!-- virtio block device driver -->
    <driver arch="i686" location="https://fedorapeople.org/groups/virt/unattended/drivers/preinst/virtio-win/0.1.141/w8/x86" pre-installable="true" signed="false">
      <file>viostor.cat</file>
      <file>viostor.inf</file>
      <file>viostor.sys</file>
      <device id="http://pcisig.com/pci/1af4/1001"/> <!-- virtio-block -->
      <device id="http://pcisig.com/pci/1af4/1042"/> <!-- virtio1.0-block-->
    </driver>

    <driver arch="x86_64" location="https://fedorapeople.org/groups/virt/unattended/drivers/preinst/virtio-win/0.1.141/w8/amd64" pre-installable="true" signed="false">
      <file>viostor.cat</file>
      <file>viostor.inf</file>
      <file>viostor.sys</file>
      <device id="http://pcisig.com/pci/1af4/1001"/> <!-- virtio-block -->
      <device id="http://pcisig.com/pci/1af4/1042"/> <!-- virtio1.0-block-->
    </driver>

    <!-- All virtio and QXL device drivers, and spice-vdagent -->
    <driver arch="i686" location="https://fedorapeople.org/groups/virt/unattended/drivers/postinst/spice-guest-tools/0.141" signed="false">
      <file>spice-guest-tools-0.141.exe</file>
      <file>spice-guest-tools-0.141.cmd</file>
      <file>qxl-0.141.cer</file>
      <file>virtio-0.141.cer</file>
      <device id="http://pcisig.com/pci/1af4/1000"/> <!-- virtio-net -->
      <device id="http://pcisig.com/pci/1af4/1001"/> <!-- virtio-block -->
      <device id="http://pcisig.com/pci/1af4/1002"/> <!-- virtio-balloon -->
      <device id="http://pcisig.com/pci/1af4/1003"/> <!-- virtio-console -->
      <device id="http://pcisig.com/pci/1af4/1004"/> <!-- virtio-scsi -->
      <device id="http://pcisig.com/pci/1af4/1005"/> <!-- virtio-rng -->
      <device id="http://pcisig.com/pci/1b36/0100"/> <!-- qxl -->
      <device id="http://pcisig.com/pci/1af4/1041"/> <!-- virtio1.0-net -->
      <device id="http://pcisig.com/pci/1af4/1042"/> <!-- virtio1.0-block-->
      <device id="http://pcisig.com/pci/1af4/1043"/> <!-- virtio1.0-console -->
      <device id="http://pcisig.com/pci/1af4/1044"/> <!-- virtio1.0-rng -->
      <device id="http://pcisig.com/pci/1af4/1045"/> <!-- virtio1.0-balloon -->
      <device id="http://pcisig.com/pci/1af4/1048"/> <!-- virtio1.0-scsi -->
      <device id="http://pcisig.com/pci/1af4/1052"/> <!-- virtio1.0-input -->
    </driver>

    <driver arch="x86_64" location="https://fedorapeople.org/groups/virt/unattended/drivers/postinst/spice-guest-tools/0.141" signed="false">
      <file>spice-guest-tools-0.141.exe</file>
      <file>spice-guest-tools-0.141.cmd</file>
      <file>qxl-0.141.cer</file>
      <file>virtio-0.141.cer</file>
      <device id="http://pcisig.com/pci/1af4/1000"/> <!-- virtio-net -->
      <device id="http://pcisig.com/pci/1af4/1001"/> <!-- virtio-block -->
      <device id="http://pcisig.com/pci/1af4/1002"/> <!-- virtio-balloon -->
      <device id="http://pcisig.com/pci/1af4/1003"/> <!-- virtio-console -->
      <device id="http://pcisig.com/pci/1af4/1004"/> <!-- virtio-scsi -->
      <device id="http://pcisig.com/pci/1af4/1005"/> <!-- virtio-rng -->
      <device id="http://pcisig.com/pci/1b36/0100"/> <!-- qxl -->
      <device id="http://pcisig.com/pci/1af4/1041"/> <!-- virtio1.0-net -->
      <device id="http://pcisig.com/pci/1af4/1042"/> <!-- virtio1.0-block-->
      <device id="http://pcisig.com/pci/1af4/1043"/> <!-- virtio1.0-console -->
      <device id="http://pcisig.com/pci/1af4/1044"/> <!-- virtio1.0-rng -->
      <device id="http://pcisig.com/pci/1af4/1045"/> <!-- virtio1.0-balloon -->
      <device id="http://pcisig.com/pci/1af4/1048"/> <!-- virtio1.0-scsi -->
      <device id="http://pcisig.com/pci/1af4/1052"/> <!-- virtio1.0-input -->
    </driver>

    <installer>
      <script id='http://microsoft.com/windows/unattend/jeos'/>
      <script id='http://microsoft.com/windows/unattend/desktop'/>

      <!--
          User avatar not being set for Windows 8 (or newer).
          For more details, please, see:
          https://bugzilla.redhat.com/show_bug.cgi?id=1328637
      -->
      <script id='http://microsoft.com/windows/cmd/desktop'/>
    </installer>
  </os>
</libosinfo>
